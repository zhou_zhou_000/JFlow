package bp.sys;
import bp.en.EntityNoAttr;
/** 
  配置信息
*/
public class EnCfgAttr extends EntityNoAttr
{
	/** 
	 分组标签
	*/
	public static final String GroupTitle = "GroupTitle";
	/** 
	 Url
	*/
	public static final String UrlExt = "UrlExt";
	/** 
	 附件路径
	*/
	public static final String FJSavePath = "FJSavePath";
	/** 
	 附件路径
	*/
	public static final String FJWebPath = "FJWebPath";
	/** 
	 数据分析方式
	*/
	public static final String Datan = "Datan";
	/** 
	 Search,Group,设置.
	*/
	public static final String UI = "UI";

	/** 
	 字段颜色设置
	*/
	public static final String ColorSet = "ColorSet";
	/** 
	 字段求和求平均设置
	*/
	public static final String FieldSet = "FieldSet";
}