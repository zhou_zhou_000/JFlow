package bp.gpm;

import bp.da.*;
import bp.web.*;
import bp.en.*;
import java.util.*;

/** 
 管理员与系统权限
*/
public class EmpAppAttr
{
	/** 
	 操作员
	*/
	public static final String FK_Emp = "FK_Emp";
	/** 
	 系统
	*/
	public static final String FK_App = "FK_App";
}